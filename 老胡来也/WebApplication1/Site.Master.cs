﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class SiteMaster : MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var se = Session["Username"];
            if (se != null)
            {
                this.lblUser.Text = "当前用户："+se.ToString();
            }
            
        }
    }
}