﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplication1._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        th{
            text-align:center;
        }
        td{
            width:200px;
            text-align:center;
        }
    </style>
    <asp:Button runat="server" ID="btnAdd" Text="添加" OnClick="btnAdd_Click"/>
    <asp:GridView runat="server" AllowPaging="true" ID="GrdList" AutoGenerateColumns="false" OnRowCancelingEdit="GrdList_RowCancelingEdit" OnPageIndexChanging="GrdList_PageIndexChanging"
        OnRowDeleting="GrdList_RowDeleting" OnRowEditing="GrdList_RowEditing" OnRowUpdating="GrdList_RowUpdating">
        <Columns>
            <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="true"/>
            <asp:BoundField DataField="TaskName" HeaderText="任务名称"/>
            <asp:BoundField DataField="TaskContent" HeaderText="任务内容"/>
            <asp:BoundField DataField="Remarks" HeaderText="备注"/>
            <asp:CommandField HeaderText="操作" ShowDeleteButton="true" ShowEditButton="true" ShowCancelButton="true"/>
        </Columns>
    </asp:GridView>

</asp:Content>
