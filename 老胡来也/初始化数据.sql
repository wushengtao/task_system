create database TaskDb
go


use TaskDb
go

create table Users
(
	Id int primary key identity,
	Username nvarchar(80) not null,
	Password nvarchar(80) not null,
	Remarks nvarchar(800) null
)

go

create table Task
(
	Id int primary key identity,
	TaskName nvarchar(80) not null,
	TaskContent nvarchar(80) not null,
	TaskStatus nvarchar(80) not null default '待执行',
	CreatedUserId int not null,
	Remarks nvarchar(800) null
)

go

insert into Users (Username,Password) values ('admin','113'),('user01','113')
go

insert into Task (TaskName,TaskContent,CreatedUserId) values ('明天就要考试啦1','html',1)
insert into Task (TaskName,TaskContent,CreatedUserId) values ('明天就要考试啦2','json',1)
insert into Task (TaskName,TaskContent,CreatedUserId) values ('明天就要考试啦3','java',1)
insert into Task (TaskName,TaskContent,CreatedUserId) values ('明天就要考试啦4','神算',1)
insert into Task (TaskName,TaskContent,CreatedUserId) values ('明天就要考试啦5','python',1)
insert into Task (TaskName,TaskContent,CreatedUserId) values ('明天就要考试啦6','C',1)
insert into Task (TaskName,TaskContent,CreatedUserId) values ('明天就要考试啦7','C++',1)
insert into Task (TaskName,TaskContent,CreatedUserId) values ('明天就要考试啦8','C#',1)
insert into Task (TaskName,TaskContent,CreatedUserId) values ('明天就要考试啦9','js',1)
insert into Task (TaskName,TaskContent,CreatedUserId) values ('明天就要考试啦10','linux',1)
insert into Task (TaskName,TaskContent,CreatedUserId) values ('明天就要考试啦11','network',1)
insert into Task (TaskName,TaskContent,CreatedUserId) values ('明天就要考试啦12','winform',1)
insert into Task (TaskName,TaskContent,CreatedUserId) values ('明天就要考试啦13','erp',1)
insert into Task (TaskName,TaskContent,CreatedUserId) values ('明天就要考试啦14','webforms',1)
insert into Task (TaskName,TaskContent,CreatedUserId) values ('明天就要考试啦15','sql',1)
insert into Task (TaskName,TaskContent,CreatedUserId) values ('明天就要考试啦16','jquery',1)
insert into Task (TaskName,TaskContent,CreatedUserId) values ('明天就要考试啦17','go',1)
insert into Task (TaskName,TaskContent,CreatedUserId) values ('明天就要考试啦18','word',1)
insert into Task (TaskName,TaskContent,CreatedUserId) values ('明天就要考试啦19','excel',1)
insert into Task (TaskName,TaskContent,CreatedUserId) values ('明天就要考试啦20','ppt',1)
insert into Task (TaskName,TaskContent,CreatedUserId) values ('明天就要考试啦21','www',1)
go

